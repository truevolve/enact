package com.truevolve.policy.enact.sample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.truevolve.enact.Interpreter;
import com.truevolve.enact.controllers.ActivityBaseController;
import com.truevolve.enact.controllers.SimpleListCheckActivity;
import com.truevolve.enact.exceptions.InterpreterException;
import com.truevolve.enact.exceptions.PolicyException;
import com.truevolve.enact.models.CheckList;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private String policy =
            "{\n" +
                    "  \"start\": {\n" +
                    "    \"type\": \"menu\",\n" +
                    "    \"on_enroll\": \"student_details\"\n" +
                    "  },\n" +
                    "  \"student_details\": {\n" +
                    "    \"type\": \"person_details\",\n" +
                    "    \"on_submit\": \"is_royalty_check\",\n" +
                    "    \"store_as\": \"person_details\",\n" +
                    "    \"display_message\": \"Please provide student details\",\n" +
                    "    \"on_back_pressed\":\"start\"\n" +
                    "  },\n" +
                    "  \"is_royalty_check\": {\n" +
                    "    \"type\": \"simple_list_checker\",\n" +
                    "    \"list\": \"royal_surnames\",\n" +
                    "    \"identifier\": \"person_details.surname\",\n" +
                    "    \"found_comment\": \"found_royalty\",\n" +
                    "    \"on_found\": \"royal_summary\",\n" +
                    "    \"on_not_found\": \"summary\",\n" +
                    "    \"on_back_pressed\":\"start\"\n" +
                    "  },\n" +
                    "  \"summary\": {\n" +
                    "    \"type\": \"summary\",\n" +
                    "    \"on_done\": \"end\",\n" +
                    "    \"name\": \"person_details.name\",\n" +
                    "    \"surname\": \"person_details.surname\",\n" +
                    "    \"title\": null,\n" +
                    "    \"display_message\": \"Welcome sudent %s %s!\",\n" +
                    "    \"on_back_pressed\":\"student_details\"\n" +
                    "  },\n" +
                    "  \"royal_summary\": {\n" +
                    "    \"type\": \"summary\",\n" +
                    "    \"on_done\": \"end\",\n" +
                    "    \"name\": \"person_details.name\",\n" +
                    "    \"surname\": \"person_details.surname\",\n" +
                    "    \"title\": \"found_royalty.comment\",\n" +
                    "    \"display_message\": \"Welcome %s %s %s, you are now a student!\",\n" +
                    "    \"on_back_pressed\":\"student_details\"\n" +
                    "  }\n" +
                    "}";

    private String aList =
            "{\n" +
                    "  \"created_at\": 1488442805002,\n" +
                    "  \"description\": \"list\",\n" +
                    "  \"display_name\": \"Royal surnames\",\n" +
                    "  \"status\": \"Active\",\n" +
                    "  \"type\": \"driver\",\n" +
                    "  \"updated_at\": 1488447045613,\n" +
                    "  \"items\": {\n" +
                    "    \"Geldenhuys\": {\n" +
                    "      \"comment\": \"His Royal Highness\",\n" +
                    "      \"created_at\": 1488446579082,\n" +
                    "      \"updated_at\": 1488447045613\n" +
                    "    },\n" +
                    "    \"Marx\": {\n" +
                    "      \"comment\": \"Her Royal Highness\",\n" +
                    "      \"created_at\": 1488446579083,\n" +
                    "      \"updated_at\": 1488447045613\n" +
                    "    },\n" +
                    "    \"Mattana\": {\n" +
                    "      \"comment\": \"His Royal Highness\",\n" +
                    "      \"created_at\": 1488446579076,\n" +
                    "      \"updated_at\": 1488447045613\n" +
                    "    },\n" +
                    "    \"Hohls\": {\n" +
                    "      \"comment\": \"His Royal Highness\",\n" +
                    "      \"created_at\": 1488446579076,\n" +
                    "      \"updated_at\": 1488447045613\n" +
                    "    },\n" +
                    "    \"Butler\": {\n" +
                    "      \"comment\": \"His Royal Highness\",\n" +
                    "      \"created_at\": 1488446579076,\n" +
                    "      \"updated_at\": 1488447045613\n" +
                    "    }\n" +
                    "  },\n" +
                    "  \"site_summary\": {\n" +
                    "    \"display_name\": \"Moonshine\",\n" +
                    "    \"uid\": \"-Kd0i7AZIf1R0BtDJ2bt\"\n" +
                    "  }\n" +
                    "}";

    private List<Class<? extends ActivityBaseController>> controllers = new ArrayList<>();
    private TextView helloText;
    private final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            controllers.add(MenuActivity.class);
            controllers.add(PersonDetailActivity.class);
            controllers.add(SummaryActivity.class);
            controllers.add(SimpleListCheckActivity.class);

            Interpreter interpreter = Interpreter.INSTANCE.setup(MainActivity.class, policy, controllers);
            interpreter.getDataStore().put("royal_surnames", new CheckList(new JSONObject(aList)));


        } catch (JSONException | PolicyException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }

        helloText = findViewById(R.id.helloText);

        findViewById(R.id.startButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Interpreter.INSTANCE.start(MainActivity.this);
                } catch (InterpreterException | JSONException e) {
                    e.printStackTrace();
                    Interpreter.INSTANCE.error(MainActivity.this, e);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: starting");

        Intent intent = getIntent();

        if (intent.hasExtra(Interpreter.END)) {
            helloText.setText(intent.getStringExtra(Interpreter.END));
        } else if (intent.hasExtra(Interpreter.ERROR)) {
            helloText.setText(intent.hasExtra(Interpreter.EXCEPTION) ?
                    intent.getStringExtra(Interpreter.EXCEPTION) : "An unknown error occurred");
        } else {
            Log.d(TAG, "onResume: no end or error!");
        }
    }
}
