package com.truevolve.policy.enact.sample.models;

/**
 * Created by Steyn Geldenhuys on 3/6/17.
 */

public class PersonDetails {
    private final String name, surname;

    public PersonDetails(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
}
