package com.truevolve.policy.enact.sample;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.truevolve.enact.DataStore;
import com.truevolve.enact.Interpreter;
import com.truevolve.enact.controllers.ActivityBaseController;
import com.truevolve.enact.exceptions.InterpreterException;
import com.truevolve.enact.exceptions.ObjectNotFoundException;
import com.truevolve.enact.exceptions.PolicyException;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;

public class SummaryActivity extends ActivityBaseController {

    private static final String NAME_CONSTRUCT = "name", SURNAME_CONSTRUCT = "surname",
            ON_DONE = "on_done", DISPLAY_MESSAGE = "display_message", TITLE = "title";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        TextView studentDetailsText = (TextView) findViewById(R.id.studentDetailsText);

        try {
            DataStore dataStore = Interpreter.INSTANCE.getDataStore();
            String name = dataStore.getProperty(getStateObj().getString(NAME_CONSTRUCT));
            String surname = dataStore.getProperty(getStateObj().getString(SURNAME_CONSTRUCT));

            String displayStr;

            if (getStateObj().has(TITLE) && !getStateObj().isNull(TITLE)) {
                String title = dataStore.getProperty(getStateObj().getString(TITLE));
                displayStr = String.format(getStateObj().getString(DISPLAY_MESSAGE), title, name, surname);

            } else {
                displayStr = String.format(getStateObj().getString(DISPLAY_MESSAGE), name, surname);
            }

            studentDetailsText.setText(displayStr);
        } catch (JSONException | ObjectNotFoundException | InvocationTargetException |
                IllegalAccessException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        Button doneButton = (Button) findViewById(R.id.doneButton);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    String stateName = getStateObj().getString(ON_DONE);
                    goToState(stateName);
                } catch (InterpreterException | JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void validate(JSONObject stateObj) throws PolicyException {

    }

    @Override
    public String getType() {
        return "summary";
    }
}
