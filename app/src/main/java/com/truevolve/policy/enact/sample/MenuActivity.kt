package com.truevolve.policy.enact.sample

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button

import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.InterpreterException
import com.truevolve.enact.exceptions.PolicyException

import org.json.JSONException
import org.json.JSONObject

class MenuActivity : ActivityBaseController() {

    override val type: String
        get() = "menu"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        val onEnroll = findViewById<View>(R.id.enrollButton) as Button

        onEnroll.setOnClickListener {
            try {
                Log.d(TAG, "onClick: doing on_enroll")

                stateObj?.getString(ON_ENROLL)?.let { goToState(it) }

            } catch (e: InterpreterException) {
                e.printStackTrace()
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    @Throws(PolicyException::class)
    override fun validate(stateObj: JSONObject) {

    }

    companion object {
        private val TAG = "MenuActivity"
        private val ON_ENROLL = "on_enroll"
    }
}
