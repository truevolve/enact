package com.truevolve.policy.enact.sample;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.truevolve.enact.DataStore;
import com.truevolve.enact.Interpreter;
import com.truevolve.enact.controllers.ActivityBaseController;
import com.truevolve.enact.exceptions.InterpreterException;
import com.truevolve.enact.exceptions.PolicyException;
import com.truevolve.policy.enact.sample.models.PersonDetails;

import org.json.JSONException;
import org.json.JSONObject;

public class PersonDetailActivity extends ActivityBaseController {

    private static final String TAG = "PersonDetailActivity";
    private EditText nameEdit, surnameEdit;
    private TextView displayMessage;

    private static final String STORE_AS = "store_as", ON_SUBMIT = "on_submit";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_details);

        displayMessage = (TextView) findViewById(R.id.displayMessage);

        try {
            displayMessage.setText(this.getStateObj().getString("display_message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        nameEdit = (EditText) findViewById(R.id.nameEdit);
        surnameEdit = (EditText) findViewById(R.id.surnameEdit);

        Button submitButton = (Button) findViewById(R.id.submitButton);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Submit pressed");


                try {
                    PersonDetails person = new PersonDetails(nameEdit.getText().toString(), surnameEdit.getText().toString());

                    DataStore dataStore = Interpreter.INSTANCE.getDataStore();

                    dataStore.put(getStateObj().getString(STORE_AS), person);

                    String stateName = getStateObj().getString(ON_SUBMIT);
                    goToState(stateName);

                } catch (JSONException | InterpreterException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public void validate(JSONObject stateObj) throws PolicyException {
    }

    @Override
    public String getType() {
        return "person_details";
    }
}
