package com.truevolve.enact.exceptions

/**
 * Created by Steyn Geldenhuys on 3/7/17.
 */

class ObjectNotFoundException(msg: String) : Exception(msg)
