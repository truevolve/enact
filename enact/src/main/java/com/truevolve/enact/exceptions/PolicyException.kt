package com.truevolve.enact.exceptions

/**
 * Created by Steyn Geldenhuys on 3/3/17.
 */

class PolicyException(message: String) : Exception(message)
