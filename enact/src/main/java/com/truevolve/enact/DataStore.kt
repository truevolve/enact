package com.truevolve.enact

import android.util.Log
import com.truevolve.enact.exceptions.ObjectNotFoundException
import java.lang.reflect.InvocationTargetException
import java.util.*

/**
 * Created by Steyn Geldenhuys on 3/7/17.
 */

class DataStore {
    private val storage: MutableMap<String, Any>

    init {
        storage = HashMap()
    }

    fun put(key: String, value: Any) {
        storage[key] = value
    }


    fun clear() {
        storage.clear()
    }


    /**
     * Provides the value of the property given a key and a property name
     *
     * @param construct the key and property specified as a '.' construct.
     * @return the value of the property
     */
    @Throws(InvocationTargetException::class, IllegalAccessException::class, NoSuchMethodException::class, ObjectNotFoundException::class)
    fun <T : Any> getProperty(construct: String): T? {
        val unstruct = construct.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        Log.d(TAG, "getProperty: construct is: $construct")
        Log.d(TAG, "getProperty: split has a length of: " + unstruct.size)

        if (unstruct.size == 1) {
            return storage[unstruct[0]] as T?
        }
        if (unstruct.isEmpty()) {
            throw ArrayIndexOutOfBoundsException("An array size of at least 1 is required.")
        }

        var obj: Any? = storage[unstruct[0]]
                ?: throw ObjectNotFoundException("Could not find object from key: " + unstruct[0])


        for (i in 1 until unstruct.size) {
            if (obj != null) {
                obj = findAndInvoke(obj, unstruct[i])
            } else {
                return null
            }
        }

        return obj as T?
    }

    @Throws(InvocationTargetException::class, IllegalAccessException::class)
    private fun findAndInvoke(obj: Any, name: String): Any? {
        for (meth in obj.javaClass.methods) {
            if (meth.name.equals(name, ignoreCase = true) || meth.name.equals("get$name", ignoreCase = true)) {

                return meth.invoke(obj)
            }
        }
        return null
    }

    fun getStorage(): Map<String, Any> {
        return storage
    }

    companion object {

        private val TAG = "DataStore"
    }
}
