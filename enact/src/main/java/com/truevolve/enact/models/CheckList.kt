package com.truevolve.enact.models

import org.json.JSONException
import org.json.JSONObject


class CheckList {

    var originalObject = JSONObject()

    var description: String = ""
    var display_name: String = ""
    var status: String = ""
    var type: String = ""
    var created_at: Long = 0
    var updated_at: Long = 0

    val items = mutableMapOf<String, Item>()

    constructor() {
        //Needed for firbase
    }

    @Throws(JSONException::class)
    constructor(job: JSONObject) {
        originalObject = job

        description = originalObject.getString(JSON_DESCRIPTION)
        originalObject.remove(JSON_DESCRIPTION)

        display_name = originalObject.getString(JSON_DISPLAY_NAME)
        originalObject.remove(JSON_DISPLAY_NAME)

        status = originalObject.getString(JSON_STATUS)
        originalObject.remove(JSON_STATUS)

        type = originalObject.getString(JSON_TYPE)
        originalObject.remove(JSON_TYPE)

        updated_at = originalObject.getLong(JSON_UPDATED_AT)
        originalObject.remove(JSON_UPDATED_AT)

        created_at = originalObject.getLong(JSON_CREATED_AT)
        originalObject.remove(JSON_CREATED_AT)

        parseItems()
    }

    @Throws(JSONException::class)
    private fun parseItems() {
        val itemsJOB = originalObject.getJSONObject(JSON_ITEMS)
        originalObject.remove(JSON_ITEMS)

        val it = itemsJOB.keys()

        while (it.hasNext()) {
            val itemName = it.next()
            val obj = itemsJOB.getJSONObject(itemName)

            items[itemName] = Item(obj)
        }
    }

    @Throws(JSONException::class)
    fun serialize(): String {
        val copyOfOriginal = JSONObject(originalObject.toString(0))

        copyOfOriginal.put(JSON_DESCRIPTION, description)
        copyOfOriginal.put(JSON_DISPLAY_NAME, display_name)
        copyOfOriginal.put(JSON_STATUS, status)
        copyOfOriginal.put(JSON_TYPE, type)
        copyOfOriginal.put(JSON_UPDATED_AT, updated_at)

        val newItems = JSONObject()
        for ((key, value) in items) {
            newItems.put(key, value.serialize())
        }

        copyOfOriginal.put(JSON_ITEMS, newItems)
        return copyOfOriginal.toString(0)
    }

    companion object {
        private val JSON_DESCRIPTION = "description"
        private val JSON_DISPLAY_NAME = "display_name"
        private val JSON_STATUS = "status"
        private val JSON_TYPE = "type"
        private val JSON_CREATED_AT = "created_at"
        private val JSON_UPDATED_AT = "updated_at"
        private val JSON_ITEMS = "items"
    }
}