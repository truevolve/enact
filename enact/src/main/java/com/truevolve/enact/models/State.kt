package com.truevolve.enact.models

import org.json.JSONObject

/**
 * Created by Steyn Geldenhuys on 3/6/17.
 */

class State(val stateController: Class<*>, val stateDescriptor: JSONObject) {
    companion object {
        const val STATE_DESCRIPTOR = "state_descriptor"
    }
}
