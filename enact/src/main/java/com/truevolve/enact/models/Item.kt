package com.truevolve.enact.models

import org.json.JSONException
import org.json.JSONObject

/**
 * Created by Steyn Geldenhuys on 3/9/17.
 */

class Item(itemObj: JSONObject? = null) {
    var comment: String? = null
    var created_at: Long? = null
    var updated_at: Long? = null

    init {
        comment = itemObj?.getString(JSON_COMMENT)
        created_at = itemObj?.getLong(JSON_CREATED_AT)
        updated_at = itemObj?.getLong(JSON_UPDATED_AT)
    }

    @Throws(JSONException::class)
    fun serialize(): JSONObject {
        val item = JSONObject()
        item.put(JSON_COMMENT, comment)
        item.put(JSON_CREATED_AT, created_at)
        item.put(JSON_UPDATED_AT, updated_at)

        return item
    }

    companion object {
        private const val JSON_COMMENT = "comment"
        private const val JSON_CREATED_AT = "created_at"
        private const val JSON_UPDATED_AT = "updated_at"
    }
}
