package com.truevolve.enact.controllers

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.truevolve.enact.Interpreter
import com.truevolve.enact.exceptions.InterpreterException
import com.truevolve.enact.models.State
import org.json.JSONException
import org.json.JSONObject

/**
 * Created by Steyn Geldenhuys on 3/3/17.
 */

abstract class ActivityBaseController : AppCompatActivity(), Controller {

    /**
     * This contains the state object as defined in the policy.
     * The stateObj automatically gets it's value in onCreate.
     */
    var stateObj: JSONObject? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (intent.hasExtra(State.STATE_DESCRIPTOR)) {
            stateObj = JSONObject(intent.getStringExtra(State.STATE_DESCRIPTOR))
        } else if (savedInstanceState?.getString(State.STATE_DESCRIPTOR) != null) {
            stateObj = JSONObject(savedInstanceState.getString(State.STATE_DESCRIPTOR))
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putString(State.STATE_DESCRIPTOR, stateObj?.toString(0))
    }

    /**
     * This method is used to transition to another state.
     *
     * @param stateName The state name to transition to.
     * @throws InterpreterException
     * @throws JSONException
     */
    @Throws(InterpreterException::class, JSONException::class)
    fun goToState(stateName: String) {

        if (stateName.equals(Interpreter.END, ignoreCase = true)) {
            Interpreter.end(this)
            return
        } else if (stateName.equals(Interpreter.ERROR, ignoreCase = true)) {
            Interpreter.error(this, null)
            return
        }

        val state = Interpreter.getState(stateName)

        if (ActivityBaseController::class.java.isAssignableFrom(state.stateController)) {
            val intent = Intent(this, state.stateController)
            intent.putExtra(State.STATE_DESCRIPTOR, state.stateDescriptor.toString(0))
            this.startActivity(intent)

        } else {
            Log.e(TAG, "goToState: ERRRRRRR type does not match")
            throw InterpreterException("$stateName is not of type ActivityBaseController")
        }
    }

    override fun onBackPressed() {
        Log.d(TAG, "onBackPressed")

        if (stateObj?.has(ON_BACK_PRESSED) == true) {
            try {
                stateObj?.getString(ON_BACK_PRESSED)?.let {
                    goToState(it)
                }
            } catch (e: InterpreterException) {
                e.printStackTrace()
                Interpreter.error(this, e)
            } catch (e: JSONException) {
                e.printStackTrace()
                Interpreter.error(this, e)
            }

        } else {
            super.onBackPressed()
        }
    }

    companion object {

        private const val TAG = "ActivityBaseController"
        private const val ON_BACK_PRESSED = "on_back_pressed"
    }
}
