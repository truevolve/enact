package com.truevolve.enact.controllers

import com.truevolve.enact.exceptions.PolicyException

import org.json.JSONObject

/**
 * Created by Steyn Geldenhuys on 3/16/17.
 */

interface Controller {

    /**
     * This method must return the controller name, AKA the type
     *
     * @return the type name
     */
    val type: String

    /**
     * This method must be implemented to validate the stateObj.
     * This method is called in the Interpreter on setup.
     *
     * @param stateObj state object to validate
     * @throws PolicyException thrown if there is an error in the state object
     */
    @Throws(PolicyException::class)
    fun validate(stateObj: JSONObject)
}
