package com.truevolve.enact.controllers

import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.widget.ProgressBar
import android.widget.TextView
import com.truevolve.enact.Interpreter
import com.truevolve.enact.exceptions.InterpreterException
import com.truevolve.enact.exceptions.PolicyException
import com.truevolve.enact.models.CheckList
import com.truevolve.enact.models.Item
import com.truevolve.policy.enact.R
import org.json.JSONObject

class SimpleListCheckActivity : ActivityBaseController() {

    private var checkList: CheckList? = null
    private var suspect: String? = null
    private var foundStateName: String? = null
    private var notFoundStateName: String? = null
    private var foundComment: String? = null
    private var progressBar: ProgressBar? = null

    override val type: String
        get() = "simple_list_checker"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_list_check)

        progressBar = findViewById(R.id.progressBar)

        try {
            val listUID = stateObj?.getString(LIST)
            suspect = stateObj?.getString(IDENTIFIER)?.let { Interpreter.dataStore.getProperty(it) }
            foundComment = stateObj?.getString(FOUND_COMMENT)
            foundStateName = stateObj?.getString(ON_FOUND)
            notFoundStateName = stateObj?.getString(ON_NOT_FOUND)

            checkList = listUID?.let { Interpreter.dataStore.getProperty(listUID) }

            val message = String.format(getString(R.string.checking_s_list), checkList?.display_name)
            val checkingListText = findViewById<TextView>(R.id.checkingListText)
            checkingListText.text = message

        } catch (e: Exception) {
            e.printStackTrace()
            Interpreter.error(this, e)
        }
    }

    override fun onResume() {
        super.onResume()

        checkList?.let { clist ->
            suspect?.let { suspect ->
                ListCheckerTask(clist, suspect).execute()
            }
        }

    }

    @Throws(PolicyException::class)
    override fun validate(stateObj: JSONObject) {
        Log.d(TAG, "validate: starting")

        if (!stateObj.has(LIST)) {
            Log.e(TAG, "validate: state object must have $LIST defined")
            throw PolicyException("state object must have $LIST defined")
        } else if (!stateObj.has(IDENTIFIER)) {
            Log.e(TAG, "validate: state object must have $IDENTIFIER defined")
            throw PolicyException("state object must have $IDENTIFIER defined")
        } else if (!stateObj.has(ON_FOUND)) {
            Log.e(TAG, "validate: state object must have $ON_FOUND defined")
            throw PolicyException("state object must have $ON_FOUND defined")
        } else if (!stateObj.has(ON_NOT_FOUND)) {
            Log.e(TAG, "validate: state object must have $ON_NOT_FOUND defined")
            throw PolicyException("state object must have $ON_NOT_FOUND defined")
        } else if (!stateObj.has(FOUND_COMMENT)) {
            Log.e(TAG, "validate: state object must have $FOUND_COMMENT defined")
            throw PolicyException("state object must have $FOUND_COMMENT defined")
        }
    }

    private inner class ListCheckerTask(private val checkList: CheckList, private val suspect: String) : AsyncTask<Void, Int, Item>() {

        override fun doInBackground(vararg params: Void): Item? {

            return checkList.items[suspect]
        }

        override fun onProgressUpdate(vararg values: Int?) {
            progressBar?.progress = values[0] ?: 0
        }

        override fun onPostExecute(result: Item?) {
            val foundComment = foundComment
            val notFoundState = notFoundStateName
            val foundState = foundStateName

            if (result == null && notFoundState != null) {
                try {
                    goToState(notFoundState)
                } catch (e: InterpreterException) {
                    e.printStackTrace()
                    Interpreter.error(this@SimpleListCheckActivity, e)
                }

            } else if (result != null && foundComment != null && foundState != null) {
                try {
                    Interpreter.dataStore.put(foundComment, result)

                    goToState(foundState)
                } catch (e: Exception) {
                    e.printStackTrace()
                    Interpreter.error(this@SimpleListCheckActivity, e)
                }

            } else {
                Interpreter.error(this@SimpleListCheckActivity, Exception("Neither found nor not found states are valid. "))
            }
        }
    }

    companion object {

        private const val LIST = "list"
        private const val IDENTIFIER = "identifier"
        private const val ON_FOUND = "on_found"
        private const val ON_NOT_FOUND = "on_not_found"
        private const val FOUND_COMMENT = "found_comment"
        private const val TAG = "SimpleListCheckActivity"
    }
}
