package com.truevolve.enact

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.InterpreterException
import com.truevolve.enact.exceptions.PolicyException
import com.truevolve.enact.models.State
import com.truevolve.policy.enact.BuildConfig
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * Created by Steyn Geldenhuys on 3/3/17.
 */

@SuppressLint("StaticFieldLeak")
object Interpreter {

    private var policy: JSONObject? = null
    private var availableControllers: Map<String, Class<out ActivityBaseController>>? = null
    val dataStore: DataStore = DataStore()
    private var caller: Class<out Activity>? = null


    @Throws(PolicyException::class, JSONException::class, InstantiationException::class, IllegalAccessException::class)
    private fun validatePolicy() {
        val keys = policy?.keys()

        val listOfTypes = ArrayList<String>()

        while (keys?.hasNext() == true) {
            val key = keys.next()

            val state = policy?.getJSONObject(key)
                    ?: throw PolicyException("$key is not a JSON Object but is required for a state.")

            if (!state.has(CONTROLLER_TYPE)) {
                throw PolicyException("$key does not have a \"type\" which is required")


            } else if (availableControllers?.containsKey(state.getString(CONTROLLER_TYPE)) == false) {
                throw PolicyException(state.getString(CONTROLLER_TYPE) + " was not found in available states.")
            }

            availableControllers?.get(state.getString(CONTROLLER_TYPE))?.let {
                validateController(it, state)
            }

            listOfTypes.add(state.getString(CONTROLLER_TYPE))
        }
    }

    @Throws(IllegalAccessException::class, InstantiationException::class, PolicyException::class)
    private fun validateController(controller: Class<out ActivityBaseController>, state: JSONObject) {
        val instance = controller.newInstance()

        instance.validate(state)
    }

    @Throws(InterpreterException::class, JSONException::class)
    fun getState(stateName: String): State {
        val state = policy?.getJSONObject(stateName)
        val controllerType = state?.getString(CONTROLLER_TYPE)

        availableControllers?.let { controllers ->
            controllerType?.let {
                controllers[it]?.let { controller ->
                    return State(controller, state)
                }
            }
        }

        throw InterpreterException("$controllerType does not exist in list of registed state controllers.")
    }

    fun hasState(stateName: String): Boolean {
        Log.d(TAG, "hasState: starting")

        return try {
            val state = policy?.getJSONObject(stateName)
            val controllerType = state?.getString(CONTROLLER_TYPE)

            availableControllers?.containsKey(controllerType) ?: false
        } catch (e: JSONException) {
            e.printStackTrace()
            false
        }
    }

    @Throws(InterpreterException::class, JSONException::class)
    fun start(context: Context) {
        val start = getState(START)
        val intent = Intent(context, start.stateController)
        intent.putExtra(State.STATE_DESCRIPTOR, start.stateDescriptor.toString(0))
        context.startActivity(intent)
    }

    fun end(context: Context) {
        Log.d(TAG, "success: YAY, we got a end!")
        dataStore.clear()

        val intent = if (caller == null) {
            context.packageManager.getLaunchIntentForPackage(BuildConfig.APPLICATION_ID)
        } else {
            Intent(context, caller)
        }

        intent.putExtra(END, END)

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        context.startActivity(intent)
    }

    fun error(context: Context, throwable: Throwable?) {
        Log.d(TAG, "fail: YAY, we got a error!")
        dataStore.clear()

        val intent = if (caller == null) {
            context.packageManager.getLaunchIntentForPackage(BuildConfig.APPLICATION_ID)
        } else {
            Intent(context, caller)
        }

        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra(ERROR, ERROR)

        if (throwable != null) {
            intent.putExtra(EXCEPTION, throwable)
        } else {
            intent.putExtra(EXCEPTION, "An unknown error occurred.")
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        context.startActivity(intent)
    }

    private const val TAG = "Interpreter"

    private const val CONTROLLER_TYPE = "type"
    const val START = "start"
    const val END = "end"
    const val ERROR = "error"
    const val EXCEPTION = "exception"

    @Throws(JSONException::class, PolicyException::class, InstantiationException::class, IllegalAccessException::class)
    fun setup(caller: Class<out Activity>, policyStr: String,
              availableStates: List<Class<out ActivityBaseController>>): Interpreter {
        this.caller = caller
        this.policy = JSONObject(policyStr)
        this.availableControllers = stateListToMap(availableStates)

        this.validatePolicy()

        return this
    }

    @Throws(IllegalAccessException::class, InstantiationException::class)
    private fun stateListToMap(availableStates: List<Class<out ActivityBaseController>>): Map<String, Class<out ActivityBaseController>> {
        val stateMap = HashMap<String, Class<out ActivityBaseController>>()

        for (state in availableStates) {
            val instance = state.newInstance()
            stateMap[instance.type] = state
        }

        return stateMap
    }
}
